const onExit = require('signal-exit')

const config = require('./config')
const gs3plus = require('./gs3plus')
const printing = require('./printing')

async function init () {
  const modem = await gs3plus.init(config.gs3plus)
  const printer = await printing.init(config.printer)

  modem.listen((sms) => {
    let number = sms.number.indexOf('+47') === 0 ? sms.number.slice(3) : sms.number
    let time = sms.date.slice(-8, -3)
    let lines = (sms.message.match(/\n/g) || []).length
    let feed = Math.max(7 - lines, 3)

    console.log(`Telegram from ${number} at ${time}: ${sms.message}`)

    printer
      .inverse(true)
      .printLine(` Telegram fra ${number} kl ${time}`.padEnd(32, ' '))
      .inverse(false)
      .prettyPrintText(sms.message)
      .lineFeed(feed)
      .print()
  })

  onExit(() => {
    modem.disconnect()
    printer.disconnect()
  })
}

try {
  init()
}
catch (err) {
  console.error(err)
}
