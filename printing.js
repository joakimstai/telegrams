const SerialPort = require('serialport')
const ThermalPrinter = require('thermalprinter')

exports.init = function (config) {
  return new Promise((resolve, reject) => {
    let port = new SerialPort(config.device, { baudRate: config.baudRate })

    port.on('open', () => {
      console.info('Connected to printer')

      let printer = new ThermalPrinter(port, config.options)

      printer.on('ready', () => {
        console.debug('Printer ready')
        resolve(printer)
      })

      printer.prettyPrintText = function (text) {
        this.printText(wrapText(text, config.columns))
        return this
      }

      printer.disconnect = () => port.close()
    })

    port.on('close', () => console.info('Disconnected from printer'))
    port.on('error', reject)
  })
}

function wrapText (text, columns) {
  let lines = ['']
  let index = 0

  text.split(' ').forEach((word) => {
    if (lines[index].length + word.length < columns) {
      lines[index] += ' ' + word
    }
    else {
      lines[index] += '\n'
      lines.push(word)
      index++
    }
  })

  return lines.join('').trim()
}
