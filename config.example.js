module.exports = {
  // SSH connection to Option GlobeSurfer 3+
  gs3plus: {
    host: '',
    port: 22,
    username: '',
    password: '',
  },

  // JP thermal printer (your printer may be different, change as necessary)
  printer: {
    device: '/dev/tty.usbserial',
    baudRate: 9600, // Differs between models
    columns: 32,
    // Options passed to the "thermalprinter" package (feel free to experiment)
    options: {
      maxPrintingDots: 15,
      heatingTime: 150,
      heatingInterval: 4,
      commandDelay: 5,
      charset: 0, // See available charsets below
      chineseFirmware: false, // Set to true if seeing Chinese characters
    },
  },
}

/**
 * Available charsets for JP thermal printer:
 * 0 - CP437 [USA, european standard]
 * 1 - KataKana [Katakana]
 * 2 - CP850 [Multi-lang]
 * 3 - CP860 [Portuguese]
 * 4 - CP863 [Canada - french]
 * 5 - CP865 [Nordic]
 * 6 - WCP1251 [Cyrillic]
 * 7 - CP866 Slavic 2
 * 8 - МИК [Slavic / Bolgarian]
 * 9 - CP755 [Eastern Europe, Latvia 2]
 * 10 - [Iran, Persian]
 * 11 - reserved
 * 12 - reserved
 * 13 - reserved
 * 14 - reserved
 * 15 - CP862 [Hebrew]
 * 16 - WCP1252 [Latin 1]
 * 17 - WCP1253 [Greeсу]
 * 18 - CP852 [Latin 2]
 * 19 - CP858 [1 + european languages, latin symbols]
 * 20 - Иран Ⅱ [Persian]
 * 21 - Latvia
 * 22 - CP864 [Arabic]
 * 23 - ISO-8859- 1 [Western Europe]
 * 24 - CP737 [Greece]
 * 25 - WCP1257 [Baltic]
 * 26 - Thai
 * 27 - CP720 [Arabic]
 * 28 - CP855
 * 29 - CP857 [Turkish]
 * 30 - WCP1250 [Central Europe]
 * 31 - CP775
 * 32 - WCP1254 [Turkish]
 * 33 - WCP1255 [Arabic]
 * 34 - WCP1256 [Arabic]
 * 35 - WCP1258 [Vietnamese]
 * 36 - ISO-8859- 2 [Latin 2]
 * 37 - ISO-8859- 3 [Latin 3]
 * 38 - ISO-8859- 4 [Baltic]
 * 39 - ISO-8859- 5 [Cyrillic]
 * 40 - ISO-8859- 6 [Arabic]
 * 41 - ISO-8859- 7 [Greece]
 * 42 - ISO-8859- 8 [Arabic]
 * 43 - ISO-8859- 9 [Turkish]
 * 44 - ISO-8859- 15 [Latin 9]
 * 45 - [Thai 2]
 * 46 - CP856
 * 47 - CP874
 *
 * Useful trick for printing cyrillic characters:
 * http://bassta.bg/2016/06/printing-cyrillic-characters-with-thermal-printer-and-node/
 */
