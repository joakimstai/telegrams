# Telegrams

Telegram receiver/printer for SMS and other short messages.

Made for the Option GlobeSurfer 3+ modem and JP TTL thermal printers, and for running on Raspberry Pi.

Requires Node.js v6.0.0+. Depends on `serialport`, `ssh2`, `thermalprinter` and `signal-exit`.
