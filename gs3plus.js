const ssh = require('ssh2')

exports.init = function (config) {
  return new Promise((resolve, reject) => {
    let client = new ssh.Client()

    client.on('ready', () => resolve(client))

    client.connect(config)

    client.listen = function (cb) {
      client.shell((err, stream) => {
        if (err) throw err

        stream.on('data', (data) => {
          if (data.indexOf('SMS') === 0) {
            parseSMS(data, cb)
          }
        })

        stream.stderr.on('data', data => console.error('Shell error:', data))

        stream.on('close', (code, signal) => {
          console.debug('Shell closed (code: ' + code + ', signal: ' + signal + ')')
          client.end()
        })

        // Enable printing of new SMSes to shell
        stream.write('modem sms_print_new\n')
      })

      return this
    }

    client.disconnect = () => client.end()

    client.on('ready', () => console.info('Connected to GlobeSurfer 3+'))
    client.on('end', () => console.info('Disconnected from GlobeSurfer 3+'))
    client.on('error', (err) => {
      if (err.code === 'ENETUNREACH' || err.code === 'EHOSTUNREACH') {
        err = `Could not connect`
      }
      console.error('GlobeSurfer 3+ error:', err.toString())
    })
  })
}

let buffer

function parseSMS (data, cb) {
  let lines = data.toString().split('\r\n')
  let first = lines.shift()

  // Extract metadata
  let sms = first.slice(4, -3).split('] ').reduce((obj, item) => {
    let [key, value] = item.split('[')
    obj[key] = value
    return obj
  }, {})

  // Remove any duplicates (bug in GS3+ shell), extract message
  let duplicates = lines.indexOf(first)
  let message = (duplicates ? lines.slice(0, duplicates) : lines).join('\n')
  sms.message = message.slice(8, -3)

  // Use a buffer that is flushed after 2 secs to roughly support multipart SMS
  if (!buffer) {
    buffer = sms

    setTimeout(() => {
      cb(buffer)
      buffer = undefined
    }, 2000)
  }
  else if (sms.number === buffer.number) {
    buffer.message += sms.message
  }
  else {
    console.error(`SMS from ${sms.number} lost due to crash with multipart buffering (${sms.date}): ${sms.message}`)
  }
}
